import { Button, Card } from '@mui/material';

export const Browse = () => {
    return (
        <>
            <Card
                sx={{
                    mt: '40vh',
                    // ml: '44vw',
                    display: 'flex',
                    flexDirection: 'row',
                    justifyContent: 'center',
                    opacity: '80%',
                    textAlign: 'center',
                    width: '100vw',
                }}
            >
                <Button href='/home'>Explore the Library</Button>
            </Card>
        </>
    );
};

export default Browse;
