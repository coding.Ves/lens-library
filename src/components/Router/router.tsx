import { createBrowserRouter } from 'react-router-dom';
import App from '../../App.tsx';
import CreateReview from '../../pages/CreateReview.tsx';
import Login from '../../pages/Login.tsx';
import NotFound from '../../pages/NotFound.tsx';
import Register from '../../pages/Register.tsx';
import Reviews from '../../pages/Reviews.tsx';
import SingleReview from '../../pages/SingleReview.tsx';
import Welcome from '../../pages/Welcome.tsx';

import Home from '../../pages/Home.tsx';
import AdminRoute from './AdminRoute.tsx';
import { Navigate } from 'react-router-dom';

const router = createBrowserRouter([
    {
        path: '/',
        element: <App />,
        children: [
            {
                // Redirect from the root path '/' to '/home'
                path: '/',
                element: <Navigate to='/home' replace />,
            },
            { path: '/home', element: <Home /> },
            {
                path: '/reviews',
                element: <Reviews />,
            },
            {
                path: 'reviews/:lensName',
                element: <SingleReview />,
            },
            {
                path: 'create-review',

                element: (
                    <AdminRoute>
                        <CreateReview />
                    </AdminRoute>
                ),
            },
        ],
    },
    {
        path: '/welcome',
        element: <Welcome />,
    },

    {
        path: '/login',
        element: <Login />,
    },
    {
        path: '/register',
        element: <Register />,
    },

    {
        path: '*',
        element: <NotFound />,
    },
]);

export default router;
