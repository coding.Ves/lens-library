import {
    Button,
    Card,
    CardActions,
    CardContent,
    CardHeader,
    CardMedia,
    Grid,
    Typography,
    Stack,
} from '@mui/material';
import { Link } from 'react-router-dom';

export const Home = () => {
    return (
        <>
            <Grid container>
                <Grid item xs={12} md={6} xl={3}>
                    <Card
                        sx={{
                            m: 3,
                            height: '600px',
                        }}
                    >
                        <CardHeader
                            title='LIBRARY OF LENSES'
                            subheader=' Vintage Lenses. Reviews. Stories.'
                            align='center'
                        />

                        <CardMedia
                            sx={{ height: '300px' }}
                            component='img'
                            image='https://firebasestorage.googleapis.com/v0/b/library-of-lenses.appspot.com/o/images%2FLens-image-top-raw_03.gif?alt=media&token=1b82e866-dc1f-4bcc-880d-511e321e1b2f'
                        ></CardMedia>
                        <CardContent>
                            <Typography>
                                This is the place to learn about different
                                vintage lenses and my experience with them. Some
                                are obscure, others quite common – from cult
                                classics to stuff with zero information online.
                            </Typography>
                        </CardContent>
                        <CardActions sx={{ justifyContent: 'center' }}>
                            <Button component={Link} to={'/reviews'}>
                                Browse Reviews
                            </Button>
                        </CardActions>
                    </Card>
                </Grid>
                <Grid item xs={12} md={6} xl={3}>
                    <Card
                        sx={{
                            m: 3,
                            height: '600px',
                        }}
                    >
                        <CardHeader
                            title='WHERE ARE WE?'
                            subheader='Whats this place?'
                            align='center'
                        />

                        <CardMedia
                            sx={{ height: '300px' }}
                            component='img'
                            image='https://live.staticflickr.com/65535/51147493422_4cd029a498_k.jpg'
                        ></CardMedia>
                        <CardContent>
                            <Typography>
                                This website is a remake of a WordPress website
                                I made some years ago about vintage lenses,
                                built from scratch using{' '}
                                <strong>
                                    React, TypeScript, Material UI, JavaScript +
                                    more.
                                </strong>
                            </Typography>
                        </CardContent>
                        <CardActions sx={{ justifyContent: 'center' }}>
                            <Button
                                href='https://www.libraryoflenses.com'
                                target='_blank'
                            >
                                Visit the original website
                            </Button>
                        </CardActions>
                    </Card>
                </Grid>
                <Grid item xs={12} md={6} xl={3}>
                    <Card
                        sx={{
                            m: 3,
                            height: '600px',
                        }}
                    >
                        <CardHeader
                            title='CONTENT'
                            subheader='Whats in here?'
                            align='center'
                        />

                        <CardMedia
                            sx={{ height: '300px' }}
                            component='img'
                            image='https://live.staticflickr.com/65535/49862511518_71f4dfeaa0_k.jpg'
                        ></CardMedia>
                        <CardContent>
                            <Typography>
                                You’ll reviews of different manual lenses from
                                the screwmount <strong>M42</strong> and{' '}
                                <strong>M39</strong>, to the SLR mounts like{' '}
                                <strong>
                                    Olympus OM, Minolta MD, Pentax PK, Canon FD,
                                    Nikon F, Contax RF, Yashica ML, Exakta
                                </strong>{' '}
                                and some others.
                            </Typography>
                        </CardContent>
                    </Card>
                </Grid>

                <Grid item xs={12} md={6} xl={3}>
                    <Card
                        sx={{
                            m: 3,
                            height: '600px',
                        }}
                    >
                        <CardHeader
                            title='QUICK LINKS'
                            subheader='Sections'
                            align='center'
                        />

                        <CardMedia
                            sx={{ height: '300px' }}
                            component='img'
                            image='https://live.staticflickr.com/65535/49862513213_7aacfbd695_k.jpg'
                        ></CardMedia>
                        <CardContent>
                            <Stack>
                                <Button
                                    variant='contained'
                                    sx={{ m: 1 }}
                                    component={Link}
                                    to={'/login'}
                                >
                                    ADMIN LOGIN
                                </Button>
                                <Button
                                    sx={{ m: 1 }}
                                    variant='contained'
                                    component={Link}
                                    to={'/create-review'}
                                >
                                    Create Review
                                </Button>
                                <Button
                                    variant='contained'
                                    sx={{ m: 1 }}
                                    component={Link}
                                    to={'/welcome'}
                                >
                                    Unused Welcome Page
                                </Button>
                            </Stack>
                        </CardContent>
                    </Card>
                </Grid>
            </Grid>
        </>
    );
};

export default Home;

// You’ll find a variety of diffferent types from the screwmount M42 and M39, to the SLR mounts like Olympus OM, Minolta MD, Pentax PK, Canon FD, Nikon F, Contax RF, Yashica ML, Exakta and some others.
