import { Box, Button, Stack } from '@mui/material';
import { NOT_FOUND_IMAGE } from '../common/constants.ts';
import { Link } from 'react-router-dom';

//<a href="https://www.freepik.com/free-vector/error-404_16446567.htm#page=7&query=404&position=5&from_view=search&track=sph">Image by GarryKillian</a> on Freepik

const NotFound = () => {
    return (
        <Stack
            component='div'
            sx={{
                display: 'flex',
                direction: 'column',
                backgroundColor: '#fff',
            }}
        >
            <Box
                component='div'
                sx={{
                    direction: 'column',
                    height: '90vh',
                    width: '100vw',
                    backgroundColor: '#fff',
                    display: 'flex',
                    backgroundImage: `url(${NOT_FOUND_IMAGE})`,
                    backgroundSize: '40%',
                    backgroundRepeat: 'no-repeat',
                    backgroundPosition: 'center',
                }}
            />
            <Button
                variant='outlined'
                sx={{
                    mb: 5,
                    width: '250px',
                    height: '50px',
                    ml: 'auto',
                    mr: 'auto',
                }}
                component={Link}
                to={'/home'}
            >
                BACK HOME
            </Button>
        </Stack>
    );
};

export default NotFound;
